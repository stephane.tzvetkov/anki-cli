"""
Anki wrapper for anki-cli.
"""

import mmap
import os

from pprint import pprint, PrettyPrinter

import click

from anki.storage import Collection
from src.profiles import ProfileManager
from src.sync import SyncManager
import src.cards_utils as cutils
import src.notes_utils as nutils
import src.sync_utils as sutils


modif_should_be_synced = False

class AnkiWrapper:
    """ Anki wrapper class. """

    def __init__(self, base=None):
        #self.modif_should_be_synced = False
        self.pm = ProfileManager(base)
        self.pm.setupMeta()
        self.setup_profile()
        if self.has_current_user_ever_succeeded_to_sync():
            #import pdb; pdb.set_trace()
            self.load_collection()
        else:
            click.echo("\nIt looks like you never identified the current user...")

    # Decks
    ############################################################################

    def list_decks(self, verbose):
        """ Print decks list info. """
        # TODO tests

        ppr = PrettyPrinter(indent=4)

        click.echo("\nDecks:\n")
        for deck in self.col.decks.all():
            click.echo(f"deck id: {deck['id']}")
            click.echo(f"deck name: {deck['name']}")
            if verbose:
                ppr.pprint(deck)
            click.echo("\n")

    def print_decks(self):
        """ Print deck(s). """
        # TODO tests
        # TODO

    # Notes
    ############################################################################

    def find_notes(self, ids, flt, raw, qry):
        """ Find filtered note(s). """
        return nutils.find_notes(self.col, ids, flt, raw, qry)

    def create_note(self, model_id, deck_id, tags, fields, note_id=None):
        """ Create a note. """
        nutils.create_note(self.col, model_id, deck_id, tags, fields, note_id)

    def delete_note(self, i):
        """ Delete a note. """
        nutils.delete_notes(self.col, [i])

    def wipe_note_tags(self, nid):
        """ . """
        note = self.col.getNote(nid)
        #nutils.wipe_note_tags(note, debug=True, col=self.col)
        nutils.wipe_note_tags(note)


    # Cards
    ############################################################################

    def find_cards(self, ids, flt, raw, qry):
        """ Find filtered card(s). """
        return cutils.find_cards(self.col, ids, flt, raw, qry)

    # Models
    ############################################################################

    def find_models(self, i):
        """ Return found model(s). """

        if i:
            for m in self.col.models.all():
                if i == m['id']:
                    return m
            return False
        else:
            models = self.col.models.all()
            return models

    def print_models(self, i):
        """ Print found model(s). """

        pprint(self.find_models(i))

    # Profiles
    ############################################################################

    def setup_profile(self) -> None:
        if self.pm.meta["firstRun"]:
            # load the new deck user profile
            self.pm.load(self.pm.profiles()[0])
            click.echo("first run")
            self.pm.meta["firstRun"] = False
            self.pm.save()

        # profile not provided on command line?
        if not self.pm.name:
            profs = self.pm.profiles()
            if len(profs) == 1:
            # if there's a single profile, load it automatically
                self.pm.load(profs[0])
            if len(profs) > 1:
            # if there's multiples profiles, chose one
                #TODO: support multiple profiles
                #diag = click.prompt('Enter the name of one of the below profiles:\n\n \
                #        %s \n\n' % profs, type=str)
                self.pm.load(profs[0])
                #self.loadProfile()
            if len(profs) == 0:
            # if there is no profile, add one
                self.add_profile()
                self.pm.load(profs[0])
        #else:
        #    self.loadProfile()

    def add_profile(self):
        name = click.prompt('Enter your profile name:\n', type=str)
        if name:
            if name in self.pm.profiles():
                return click.echo("Warning: name exists!")
            self.pm.create(name)
            self.pm.name = name

    # Collection load/unload
    ############################################################################

    def load_collection(self) -> bool:
        try:
            cpath = self.pm.collectionPath()
            #self.col = Collection(cpath)
            self.col = Collection(path=cpath, lock=False, server=True)
            return True
        except Exception as e:
            click.secho(f"Anki was unable to open your collection file; please try again.\n\n"
                        f"Debug info: {str(e)}", fg="red")
            # clean up open collection if possible:
            if self.col:
                try:
                    self.col.close(save=False)
                except:
                    pass
                self.col = None

            raise click.Abort()

    # Syncing
    ############################################################################

    def has_current_user_ever_succeeded_to_sync(self):
        """ Find if the current user ever succeeded to sync. """

        collection_log_path = os.path.join(self.pm.base, self.pm.name, "collection.log")
        if os.path.isfile(collection_log_path) is not True:
            return False

        # cf. https://stackoverflow.com/questions/4940032/how-to-search-for-a-string-in-text-files
        with open(collection_log_path, 'rb', 0) as file, \
            mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as string:
            if string.find(b'sync.py:sync()') != -1:
                return True
        return False

    def sync(self, auth=None, cancel=None, download=None, upload=None):
        """
        Login to an ankiweb profile
        """

        if auth is not None:
            self.pm.profile['syncUser'] = auth[0]
        elif self.pm.profile['syncKey'] is None and auth is None:
            click.secho('\nAccount required!', bold=True)
            click.echo("\n"
                       +"A free account is required to keep your collection synchronized.\n"
                       +"Please sign up here: https://ankiweb.net/account/login "
                       +"if you havn't already.\n\n"
                       +"Then enter your details below:")

            auth = ask_user_credentials()
            self.pm.profile['syncUser'] = auth[0]

        syncer = SyncManager(self.pm, cancel, download, upload)

        syncer.sync(auth)
        #sutils.sync(self.pm, auth)
        self.load_collection()

def ask_user_credentials():
    """ Ask user e-mail and password for first sync. """

    import getpass # pylint: disable=C0415 # disable import-outside-toplevel

    uid = input("[anki] user e-mail: ")
    pwd = getpass.getpass("[anki] password for "+uid+": ")
    if not uid or not pwd:
        click.secho("\nYou didn't provided your e-mail and/or your password"
                    +"; please try again.\n", fg="red")
        raise click.Abort()
    return (uid, pwd)
