"""
Click custom utility classes.
"""

from click import Option, UsageError

class MutuallyExclusiveOption(Option):
    """
    This class defines mutualy exclusive options for Click:
    https://stackoverflow.com/questions/37310718/mutually-exclusive-option-groups-in-python-click
    """

    def __init__(self, *args, **kwargs):
        self.mutually_exclusive = set(kwargs.pop('mutually_exclusive', []))
        ## Hide auto include in help:
        #hlp = kwargs.get('help', '')
        #if self.mutually_exclusive:
        #    ex_str = ', '.join(self.mutually_exclusive)
        #    kwargs['help'] = hlp + (
        #        ' NOTE: This argument is mutually exclusive with '
        #        ' the following options: [' + ex_str + '].'
        #    )
        super(MutuallyExclusiveOption, self).__init__(*args, **kwargs)

    def handle_parse_result(self, ctx, opts, args):
        if self.mutually_exclusive.intersection(opts) and self.name in opts:
            raise UsageError(
                "Illegal usage: `{}` is mutually exclusive with "
                "the following option(s)`{}`.".format(
                    self.name,
                    ', '.join(self.mutually_exclusive)
                )
            )

        return super(MutuallyExclusiveOption, self).handle_parse_result(
            ctx,
            opts,
            args
        )

class Map(dict):
    """
    This class allow to access the CONTEXT_SETTINGS outside of the Click framework:
    https://stackoverflow.com/questions/2352181/how-to-use-a-dot-to-access-members-of-dictionary
    """
    def __init__(self, *args, **kwargs):
        super(Map, self).__init__(*args, **kwargs)
        for arg in args:
            if isinstance(arg, dict):
                for k, v in arg.items():
                    self[k] = v

        if kwargs:
            for k, v in kwargs.items():
                self[k] = v

    def __getattr__(self, attr):
        return self.get(attr)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        super(Map, self).__setitem__(key, value)
        self.__dict__.update({key: value})

    def __delattr__(self, item):
        self.__delitem__(item)

    def __delitem__(self, key):
        super(Map, self).__delitem__(key)
        del self.__dict__[key]
