"""
Cards utility functions for the Anki-wrapper.
"""

from pprint import pprint
import click

# TODO add s - slt - silent - option to the find_cards function
def find_cards(col, ids=False, flt=None, raw=False, qry=None, verbose=False, silent=False):
    """ Find filtered card(s). """

    if col is None:
        click.secho("\nError:\n"
                    +"No collection has been given, nothing can be found.\n")
        raise click.Abort()

    if flt:
        if not flt.find(';'):
            click.secho("If you want to execute an sql command, "
                        +"please do not try to insert one here, "
                        +"use the 'execute' command instead! "
                        +"Run `$ anki-cli execute -h` for more details.", fg="red")
            raise click.Abort()
    else:
        flt = ""

    if ids:
        query = f"select id from cards {flt}"
        if verbose and not silent:
            click.echo(f"\nCard(s) ids fund with the following query: `{query}`\n")
        results = col.db.list(query)
        if not silent:
            pprint(results)
            click.echo("\n")
        return results

    elif qry:
        results = col.findCards(qry)
        if verbose and not silent:
            click.echo(f"\nAnki-card(s) fund with the following Anki embedded query: `{qry}`\n")
        cards_list = []
        for i in results:
            card = col.getCard(i)
            cards_list.append(card)
            if not silent:
                print_card(card)
        return cards_list

    elif raw:
        query = f"select * from cards {flt}"
        if verbose and not silent:
            click.echo(f"\nRaw DB card(s) fund with the following query: `{query}`\n")
        results = col.db.execute(query)
        results_list = []
        for i in results:
            results_list.append(i)
            if not silent:
                click.echo("card:")
                pprint(i)
                click.echo("\n")
        return results_list

    else:
        query = f"select id from cards {flt}"
        if verbose and not silent:
            click.echo(f"\nAnki-card(s) fund with the following query: `{query}`\n")
        results = col.db.list(query)
        cards_list = []
        for i in results:
            card = col.getCard(i)
            cards_list.append(card)
            if not silent:
                print_card(card)
        return cards_list

def print_card(card):
    """ Print a card. """

    click.echo(f"card id: {card.id}")
    click.echo(f"{{'id': {str(card.id)},\n"
               f" 'nid': {str(card.nid)},\n"
               f" 'did': {str(card.did)},\n"
               f" 'ord': {str(card.ord)},\n"
               f" 'mod': {str(card.mod)},\n"
               f" 'usn': {str(card.usn)},\n"
               f" 'type': {str(card.type)},\n"
               f" 'queue': {str(card.queue)},\n"
               f" 'due': {str(card.due)},\n"
               f" 'ivl': {str(card.ivl)},\n"
               f" 'factor': {str(card.factor)} ,\n"
               f" 'reps': {str(card.reps)},\n"
               f" 'lapses: {str(card.lapses)},\n"
               f" 'left: {str(card.left)},\n"
               f" 'odue: {str(card.odue)},\n"
               f" 'odid: {str(card.odid)},\n"
               f" 'flags': {str(card.flags)},\n"
               f" 'data': {str(card.data)},\n"
               f" '[additional] mid': {str(card.model()['id'])} }}\n")
