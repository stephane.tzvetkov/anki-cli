"""
Anki-cli
"""

import atexit
import os
from pathlib import Path

import click
import src.ankiwrap as ankiwrap
from src.ankiwrap import AnkiWrapper
from src.click_utils import MutuallyExclusiveOption, Map

import src.notes_utils as nutils

DOCS_PATH = Path(os.path.realpath(__file__)).parent.parent / "docs"
CONTEXT_SETTINGS = dict(
    obj={
        'base': os.path.join(Path.home(), '.local/share/Anki2'), # default base path
        #'base': os.path.join(Path.home(), 'projects/anki-cli/tests/tests-base'), # tests base path
        #'anki': AnkiWrapper()
    },
    #help_option_names=['-h', '--help'],
)

def doc_to_help(doc_file_md):
    help_page = str(Path(doc_file_md).read_text()
                    .replace("???tip ", "")
                    .replace("???+tip ", "")
                    .replace("!!!abstract ", "")
                    .replace("!!!+abstract ", "")
                    .replace("\n", "\n\t")
                    #.replace("SYNOPSIS", "\b\b\b\b\b\b\b\bSYNOPSIS")
                    #.replace("OVERVIEW", "\b\b\b\b\b\b\b\bOVERVIEW")
                    #.replace("DESCRIPTION", "\b\b\b\b\b\b\b\bDESCRIPTION")
                    #.replace("OPTIONS", "\b\b\b\b\b\b\b\bOPTIONS")
                    #.replace("EXAMPLES", "\b\b\b\b\b\b\b\bEXAMPLES")
                    #.replace("DETAILS", "\b\b\b\b\b\b\b\bDETAILS")
                    #.replace("NOTES", "\b\b\b\b\b\b\b\bNOTES")
                    .replace("#!Bash ", "$ ")
                    .replace("#!MySQL ", "")
                    .replace("<br />", "")
                    .replace("#### ", "\b\b\b\b\b\b\b\b")
                    .replace("### ", "\b\b\b\b\b\b\b\b")
                    .replace("## ", "\b\b\b\b\b\b\b\b")
                    .replace("# ", "\b\b\b\b\b\b\b\b"))
    help_page = "\nNAME\n"+help_page
    return help_page

@atexit.register
def cleanup():
    """
    Last function to be run (cleanup), after Click commands,
    in order to commit the changes and close the collection database.
    """
    # TODO an error has occured, no changes has been commit! (rollback?)
    map_ctx = Map(CONTEXT_SETTINGS)
    if "anki" in map_ctx.obj:
        anki = map_ctx.obj['anki']
        if ankiwrap.modif_should_be_synced is True:
            anki.col.db.commit()
            click.secho("\nModifications have been committed to your local collection database."
                        , fg="green")
            if anki.pm is not None and anki.pm.profile['syncKey']:
                click.secho("Remember to sync!\n", fg="yellow", bold=True)

            anki.col.close()
        elif anki.col and anki.col.db:
            anki.col.db.commit()
            anki.col.db.close()

@click.group(context_settings=CONTEXT_SETTINGS)
@click.option('-b', '--base', type=click.Path(), help="Path to Anki base directory.")
@click.pass_context
def main(ctx, base):
    """
    Anki-cli aims to be a command line interface alternative to the Anki desktop application.

    Anki is a free and open-source flashcard program that utilizes spaced repetition in order to
    memorize many things easily (cf. https://en.wikipedia.org/wiki/Anki_(software) and
    https://apps.ankiweb.net/index.html).

    \f
    Main function / entry point of anki-cli.

    """

    #ctx.ensure_object(dict) # ensure that ctx.obj exists and is a dict

    if base:
        ctx.obj['base'] = base

    ctx.obj['anki'] = AnkiWrapper(base)

@main.command()
@click.pass_context
@click.option('-i', '--id', 'user',
              help="Indicate ankiweb user id (for first sync only). "
              +"Note that this option will be ignored if not used with the below password option.")
#TODO: improve password help
@click.option('-p', '--password',
              help="Indicate ankiweb user password (for first sync only). "
              +"/!\\ Note that this option is recommanded only if you want to use it for a script "
              +"that **realy** needs to hardcode your password (in this cause make sure to not "
              +"share this script and to use a not sensitive password). If you are not part of "
              +"this use case: then don't run this option, a safe cli prompt will pop-up in time "
              +"and will handle your crendentials./!\\")
@click.option('-c', '--cancel', is_flag=True,
              help="If asked to download from ankiweb or upload to ankiweb: auto cancel.")
@click.option('-d', '--download', is_flag=True,
              help="If asked to download from ankiweb or upload to ankiweb: auto download.")
@click.option('-u', '--upload', is_flag=True,
              help="If asked to download from ankiweb or upload to ankiweb: auto upload.")
def sync(ctx, user, password, cancel, download, upload):
    """ Sync anki to your base directory """

    anki = ctx.obj['anki']
    if user is not None and password is not None:
        auth = (user, password)
    else:
        auth = None

    anki.sync(auth, cancel, download, upload)

@main.command()
@click.option('-v', '--verbose', is_flag=True, help="Print more decks details.")
@click.pass_context
def list_decks(ctx, verbose):
    """ Print decks list info """

    anki = ctx.obj['anki']
    anki.list_decks(verbose)

# TODO: verbose mode (without verbose print raw results)
# TODO: rename filter to query and query to anki-query
# TODO: replace -f --filter by -w --where
# TODO: make sure that the help examples are correct
@main.command()
@click.option('-i', '--ids', is_flag=True,
              cls=MutuallyExclusiveOption, mutually_exclusive=["raw", "query"])
@click.option('-f', '--filter', 'flt',
              cls=MutuallyExclusiveOption, mutually_exclusive=["query"])
@click.option('-r', '--raw', is_flag=True,
              cls=MutuallyExclusiveOption, mutually_exclusive=["ids", "query"])
@click.option('-q', '--query',
              cls=MutuallyExclusiveOption, mutually_exclusive=["ids", "flt", "raw"])
@click.option('-h', '--help', 'hlp', is_flag=True)
@click.pass_context
def find_notes(ctx, ids, flt, raw, query, hlp):
    """
    find-note command

    For more details:
    * cf. src/note_utils.py -> find_notes(...)
    * cf. doc/commands/find_notes.md
    """
    if hlp:
        find_notes_md = DOCS_PATH / "commands" / "find-notes.md"
        click.echo(doc_to_help(find_notes_md))
    else:
        anki = ctx.obj['anki']
        anki.find_notes(ids, flt, raw, query)

@main.command()
@click.option('-m', '--model',
              help="Specify model name or model id for the note to be created. "
                   "If not specified, the default 'Basic' model will be used")
@click.option('-d', '--deck', required=True,
              help="Specify deck name or deck id for the note to be created.")
@click.option('-t', '--tags', multiple=True,
              help="Specify one or multiple tag(s) for the note to be created.\n"
              +"E.g. `$ anki-cli -m test-model -d test-deck -t first-tag -t 'second tag'`.")
@click.option('-f', '--fields', multiple=True,
              help="Specify one or multiple field(s) for the note to be created.\n"
              +"E.g. `$ anki-cli -m test-model -d test-deck -f first-field -f 'second field'`.")
@click.pass_context
def add_note(ctx, model, deck, tags, fields):
    """ Create a note. """
    # TODO WIP

    anki = ctx.obj['anki']
    #anki.create_note(model, deck, tags, fields)
    #nutils.add_note(col=anki.col, flds=fields, nid=)

@main.command()
@click.option('-i', '--id', 'i', required=True,
              help="Specify note id to delete.")
@click.pass_context
def del_note(ctx, i):
    """ Create a note. """
    # TODO WIP

    anki = ctx.obj['anki']
    anki.delete_note(i)

@main.command()
@click.option('-i', '--ids', is_flag=True,
              cls=MutuallyExclusiveOption, mutually_exclusive=["flt", "raw", "query"],
              help="Only return notes id list.")
@click.option('-f', '--filter', 'flt',
              cls=MutuallyExclusiveOption, mutually_exclusive=["ids", "query"],
              help="Filter the card(s) you want to find (cf. --help for more details).")
@click.option('-r', '--raw', is_flag=True,
              cls=MutuallyExclusiveOption, mutually_exclusive=["ids", "query"],
              help="Find raw notes results, not anki-notes objects results.")
@click.option('-q', '--query',
              cls=MutuallyExclusiveOption, mutually_exclusive=["ids", "flt", "raw"],
              help="Filter the note(s) you want to find using the Anki embedded query system "
              +"(cf. --help for more details).")
@click.pass_context
def find_cards(ctx, ids, flt, raw, query):
    """ Find filtered card(s).

        This command will search in your anki collection database (DB) for card(s).

        \b
        By default, it will execute the following sql query against your DB:
        `select id from cards`

        \b
        The content of the find-cards filter (-f, --filer option) will extend this query,
        e.g. `$ anki-cli find-card -f "where id = 1585325230733"`
        will execute `select id from cards where id = 1585325230733`

        \b
        With this query: `select id from cards`, one would expect find-cards to just print the id
        of every card.
        But actualy, with this id, find-card will find the associated "anki-card" object
        and print it's full content (wich has more details than the `select * from cards` query,
        e.g. it will also print the assiociated model id of the card).

        \b
        If however, you rather prefer the raw DB result of the `select * from cards` query,
        you can execute it instead of `select id from cards` with the -r (--raw) option.

	\b
        Note that Anki embeds its own DB query system, cf. https://docs.ankiweb.net/#/searching.
	These queries are also supported with the -q (--query) option.


        Examples of filters...

        \b
        ...by available column:
            - no filter will print all cards
                `$ anki-cli find-cards`
            - [id]
                `$ anki-cli find-cards -f "where id = 1585325230733"`
            - [nid]
                `$ anki-cli find-cards -f "where nid = 1585325230732"`
            - [did]
                `$ anki-cli find-cards -f "where did = 1585325230732"`
            - [ord]
                `$ anki-cli find-cards -f "where ord >= 0"`
            - [mod]
                `$ anki-cli find-cards -f "where ord = 1585325230"`
            - [usn]
                `$ anki-cli find-cards -f "where usn != 1"`
            - [type]
                `$ anki-cli find-cards -f "where type < 3"`
            - [queue]
                `$ anki-cli find-cards -f "where queue >= 1"`
            - [due]
                `$ anki-cli find-cards -f "where due >= 1"`
            - [ivl]
                `$ anki-cli find-cards -f "where ivl = 0"`
            - [factor]
                `$ anki-cli find-cards -f "where factor <= 2"`
            - [reps]
                `$ anki-cli find-cards -f "where reps >= 0"`
            - [lapses]
                `$ anki-cli find-cards -f "where lapses != 0"`
            - [left]
                `$ anki-cli find-cards -f "where left = 6"`
            - [odue]
                `$ anki-cli find-cards -f "where odue <= 42"`
            - [odid]
                `$ anki-cli find-cards -f "where odid >= 36"`
            - [flags]
                `$ anki-cli find-cards -f "where flags = 0"`
            - [data]
                `$ anki-cli find-cards -f "where data like '%data content%'"
                `$ anki-cli find-cards -f "where instr(data, 'data content') > 0"

        \b
        ...mixing columns:
                `$ anki-cli find-note -f "where data like '%data content%' or flags = 42 "`
                `$ anki-cli find-note -f "where flags = 42 and usn = 4"
                `$ anki-cli find-note -f "where flags >= 42 or usn <= 4"
                `$ anki-cli find-note -f "where flags >= 42 and usn <= 4"

        \b
        ...ordering the results:
                `$ anki-cli find-note -f "where flags >= 0 order by flags, id DESC"
                `$ anki-cli find-note -f "where flags < 10 order by nid, usn ASC"

        \b
        There is a lot of ways to create more complex filters.
        If you want to learn how, you can start
        here: https://www.w3schools.com/SQL/default.asp
        and here: https://www.tutorialspoint.com/sql/index.htm

        \b
        So far, the examples given here are rather simplistic. Feel free to propose me more
        practical examples to improve this help page.

    """

    anki = ctx.obj['anki']
    anki.find_cards(ids, flt, raw, query)

@main.command()
@click.option('-i', '--id', 'i',
              help="Only print the model associated to the given id, if any.")
@click.pass_context
def print_models(ctx, i):
    """ Print your collection models. """

    anki = ctx.obj['anki']
    anki.print_models(i)


@main.command()
#@click.option('--n', default=1, show_default=True, required=True, type=int)
#@click.option('--password', prompt="Password:", hide_input=True, confirmation_prompt=True)
@click.option('-b', '--blah')
@click.option('-p', '--plop', cls=MutuallyExclusiveOption, mutually_exclusive=["test", "blah"])
#@click.option('-t', '--test', cls=MutuallyExclusiveOption, mutually_exclusive=["plop"])
@click.option('-t', '--test', cls=MutuallyExclusiveOption)
@click.pass_context
def test(ctx, blah, plop, test):
    """ Cli test command """
    anki = ctx.obj['anki']
    #anki.wipe_note_tags(1)
    #anki.col.db.commit()
    click.secho("test", fg="red")
    raise click.Abort()
