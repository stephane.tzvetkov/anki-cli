"""
Parse utility functions for the Anki-wrapper.
"""

import json
import click
import src.notes_utils as nutils

# TODO add auto-id option, and/or auto-id block (to parse), in order for the parser to auto-write
#      an id in the file

def dict_raise_on_duplicates(ordered_pairs):
    """
    Reject duplicate keys by raising an error, when loading a json string
    (cf. https://stackoverflow.com/a/14902564).
    """
    d = {}
    for k, v in ordered_pairs:
        if k in d:
            raise ValueError("duplicate key: %r" % (k,))
        d[k] = v
    return d

FPATH = ""
def parse_file(col, file_path, start_delim="<!--", end_delim="-->", verbose=False, silent=False):
    """
    Parse a file in order to create/update decks, models, notes, cards etc.
    """
    fpath = file_path
    is_parsing = False
    to_parse = ""
    parse_type = ""
    line_nb = 0
    for line in open(file_path, 'r'):
        for word in line.split():
            if word.find(start_delim) > -1 and word.find(end_delim) > -1:
                click.secho(f"\nError (file '{FPATH}', arround line {line_nb}):\n"
                            f"A block's end-delimiter (e.g. default '-->') and a block's "
                            f"start-delimiter (e.g. '<!--note') are next to each other: separate "
                            f"them by a space, a tabulation or a line return, for the parser to "
                            f"work!", fg="red")
                raise click.Abort()
            elif word.find(start_delim) > -1:
                parse_type = word.replace(start_delim, "")
            elif word.find(end_delim) > -1:
                word = word.replace(end_delim, "")
                to_parse += " "+word
                if to_parse and parse_type:
                    _exe_parse(col, line_nb, to_parse, parse_type)
                    to_parse = ""
                    parse_type = ""
                else:
                    click.secho(f"\nError (file '{FPATH}', arround line {line_nb}):\n"
                                f"A block's end-delimiter (e.g. default is '-->') has been "
                                f"detected without associated block's start-delimiter (e.g. "
                                f"'<!--note'), or without any type in the associated block's "
                                f"start-delimiter (e.g. '<!--' instead of '<!--note')!", fg="red")
                    raise click.Abort()
            else:
                to_parse += " "+word
        line_nb += 1

def _exe_parse(col, line_nb, to_parse, parse_type):
    """
    Execute parsing depending on the type of the bloc to parse.
    """
    parse_type = parse_type.strip(' \t\n\r')
    if parse_type == "note":
        _exe_parse_note(col, line_nb, to_parse)
    elif parse_type == "card":
        pass
    else:
        click.secho(f"\nError (file '{FPATH}', line around {line_nb}):\n"
                    f"Unknow parsing block type: '{parse_type}'!", fg="red")
        raise click.Abort()

def _exe_parse_note(col, line_nb, to_parse):
    """
    Execute note parsing.
    """
    note_json = json.loads(to_parse, object_pairs_hook=dict_raise_on_duplicates)
    nid = mid = did = cid = tags = flds = None
    for key in iter(note_json):
        if key == 'id':
            nid = note_json['id']
        elif key == 'mid':
            mid = note_json['mid']
        elif key == 'did':
            did = note_json['did']
        elif key == 'cid' in note_json:
            cid = note_json['cid']
        elif key == 'tags':
            tags = note_json['tags']
        elif key == 'flds':
            flds = note_json['flds']
        else:

            click.secho(f"\nError (file '{FPATH}', arround line {line_nb}):\n"
                        f"Unknow json key: '{key}'! Known keys for note json parsing are 'id', "
                        f"'mid', 'did', 'cid', 'tags' and 'flds'.", fg="red")
            raise click.Abort()

    if not nid:
        click.secho(f"\nError (file '{FPATH}', arround line {line_nb}):\n"
                    f"", fg="red")
        raise click.Abort()

    if nutils.find_notes(col=col, ids=True, flt=f"where id = {nid}", slt=True):
        nutils.upd_note(col=col, nid=nid, mid=mid, did=did, tags=tags, flds=flds)
    else:
        nutils.add_note(col=col, flds=flds, nid=nid, mid=mid, did=did, tags=tags)
