"""
Anki-cli tests.
"""

#import os
#import shutil
from datetime import datetime
#from pathlib import Path

import pytest
from click.testing import CliRunner
# click testing: https://click.palletsprojects.com/en/7.x/testing/

# pylint: disable=W0611 # disable "unused import"
#from global_fixtures import 
# pylint: enable=W0611 # enalbe back "unused import"

from src.anki_cli import main

# TODO test with an empty base

#def test_sync(tests_base):
def sync(tests_base):
    """
    Test anki-cli sync command.
    """
    runner = CliRunner()
    result = runner.invoke(main, ["--base", tests_base, "sync", "-u"])
    assert result.exit_code == 0

    # TODO
    # Improve this test by syncing/uploading a random note, removing it locally,
    # and syncing/downloading it back: make sure the random note is finally present!

    # TODO
    # This test could be improved by checking a successful sync in the collection.log file.
    # Unfortunately, somehow, it seems that this file is filled by the anki submodule after the
    # test. I can't find how, but here is what I tried:

    now_date = datetime.now()
    now_secs = int(now_date.timestamp())
    sync_success = "["+str(now_secs)+"] sync.py:sync()"
    print(open(tests_base / "User 1" / "collection.log", 'r').read().find(sync_success))

    import mmap
    # cf. https://stakoverflow.com/a/4944929
    with open(tests_base / "User 1" / "collection.log", 'rb', 0) as file, \
    	mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as string:
        print(string)
        if string.find(str.encode(sync_success)) != -1:
            print('true')

#@pytest.mark.sync
#@pytest.mark.first
#@pytest.mark.usefixtures("wipe_tests_base") # Wipe any pre-existing tests-base.
#def test_first_sync(tests_base, test_id, test_pwd):
def first_sync(tests_base, test_id, test_pwd):
    """
    Test anki-cli sync command when syncing for the first time.
    """
    runner = CliRunner()
    #with runner.isolated_filesystem():
    result = runner.invoke(main, ["--base", tests_base,
                                  "sync",
                                  "-d",
                                  "-i", test_id,
                                  "-p", test_pwd])
    assert result.exit_code == 0

    #import os
    #arr = os.listdir(tests_base)
    #print(arr)
    #assert False
    #
    # print -> ['prefs21.db', 'User 1']

    # TODO
    #
