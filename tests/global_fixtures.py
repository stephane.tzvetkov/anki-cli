"""
Fixtures available for all tests in this directory.
"""

import os
import shutil
from pathlib import Path

import pytest
from src.ankiwrap import *

TESTS_PATH = Path(os.path.realpath(__file__)).parent
DEFAULT_ANKI_BASE_PATH = TESTS_PATH / "default-anki-base"
TESTS_BASE_PATH = TESTS_PATH / "tests-base"

#@pytest.fixture(scope="session")
#def wipe_tests_base():
#    """
#    Wipe the pytest-base.
#    """
#    if TESTS_BASE_PATH.is_dir():
#        shutil.rmtree(TESTS_BASE_PATH)
#    os.makedirs(TESTS_BASE_PATH)

#@pytest.fixture(scope="session")
#def replace_tests_base():
#    """
#    Replace the test-base for anki-cli with the content of the default-anki-base.
#    """
#    if TESTS_BASE_PATH.is_dir():
#        shutil.rmtree(TESTS_BASE_PATH)
#    if not DEFAULT_ANKI_BASE_PATH.is_dir():
#        pytest.fail(f"The {DEFAULT_ANKI_BASE_PATH} directory has been removed!")
#    shutil.copytree(DEFAULT_ANKI_BASE_PATH, TESTS_BASE_PATH)
#    #Path(base).mkdir(parents=True, exist_ok=True)

@pytest.fixture(scope="session")
def tests_base():
    """
    Create a test-base for anki-cli out of the default-anki-base.
    """
    if TESTS_BASE_PATH.is_dir():
        shutil.rmtree(TESTS_BASE_PATH)
    if not DEFAULT_ANKI_BASE_PATH.is_dir():
        pytest.fail(f"The {DEFAULT_ANKI_BASE_PATH} directory has been removed!")
    shutil.copytree(DEFAULT_ANKI_BASE_PATH, TESTS_BASE_PATH)
    yield TESTS_BASE_PATH

@pytest.fixture(scope="session")
def ankiwrap(tests_base):
    """
    Provide anki wrapper.
    """
    anki = AnkiWrapper(tests_base)
    yield anki

@pytest.fixture(scope="session")
def db(ankiwrap):
    """
    Provide db connection.
    """
    yield ankiwrap.col.db

@pytest.fixture(scope="session")
def cursor(db):
    """
    Provide connection cursor to the anki db.
    """
    yield db.cursor()


@pytest.fixture(scope="session")
def sql_deck(ankiwrap, cursor):
    """
    Insert a deck to be tested, then delete it after tests.
    """
    cursor.execute('select decks from col')
    decks_before_all_inserts = cursor.fetchall()[0][0] # in anki db: there is only one 'col' raw

    def _deck(did, decks_before_current_insert=None):
        if did == 1:
            pytest.fail("The \"Default\" deck, with did 1, can't be overwritten!")

        if decks_before_current_insert:
            decks = decks_before_current_insert[:-1] # remove last '}' to insert the following deck
        else:
            decks = decks_before_all_inserts[:-1] # remove last '}' to insert the following deck

        deck_to_insert = (f', "{did}": '
                          f'{{'
                          f'"newToday": [0, 0], '
                          f'"revToday": [0, 0], '
                          f'"lrnToday": [0, 0], '
                          f'"timeToday": [0, 0], '
                          f'"conf": 1, '
                          f'"usn": -1, '
                          f'"desc": "", '
                          f'"dyn": 0, '
                          f'"collapsed": false, '
                          f'"extendNew": 10, '
                          f'"extendRev": 50, '
                          f'"id": {did}, '
                          f'"name": "DeckTest{did}", '
                          f'"mod": 0'
                          f'}}')
        decks += deck_to_insert
        decks += " }"
        cursor.execute(f"update col set decks = '{decks}';")

        cursor.execute(f"select dconf from col;")
        dconf = cursor.fetchone()[0]
        ankiwrap.col.decks.load(decks, dconf) # update anki's collection decks

    yield _deck

    cursor.execute(f"update col set decks = '{decks_before_all_inserts}';")

@pytest.fixture(scope="session")
def sql_decks(cursor, sql_deck):
    """
    Insert decks to be tested, and delete them after tests.
    """
    dids = [2, 3, 4, 5]
    for i in dids:
        cursor.execute('select decks from col')
        decks = cursor.fetchall()[0][0] # in anki db: there is only one 'col' raw
        sql_deck(i, decks)

    return dids

@pytest.fixture
def sql_card(cursor):
    """
    Insert card to be tested, and delete it after tests.
    """
    inserted_cards = []

    def _card(cid=1, nid=1):
        did = 1 # 'Default' anki deck id
        due = 1

        c_ord = mod = c_type = queue = ivl = factor = reps = lapses = left = odue = odid = 0
        usn = -1

        # Anki-2.1.16 do not use the 'flags' and 'data' fields
        flags = 0
        data = ""

        sql_card_to_insert = f"""insert into
 cards (   id,   nid,   did,    ord,    mod,   usn,     type,   queue,   due,   ivl,   factor,
          reps,   lapses,   left,   odue,   odid,   flags,    data
       )
 values( {cid}, {nid}, {did}, {c_ord}, {mod}, {usn}, {c_type}, {queue}, {due}, {ivl}, {factor},
         {reps}, {lapses}, {left}, {odue}, {odid}, {flags}, '{data}'
       )
    """

        sql_card = {"id": cid, "nid": nid, "did": did, "ord": c_ord, "mod": mod, "usn": usn,
                    "type": c_type, "queue": queue, "due": due, "ivl": ivl, "factor": factor,
                    "reps": reps, "lapses": lapses, "left": left, "odue": odue, "odid": odid,
                    "flags": flags, "data": data}

        inserted_cards.append(sql_card)
        cursor.execute(sql_card_to_insert)
        return sql_note

    yield _card

    for card in inserted_cards:
        cursor.execute(f"delete from cards where id = {card['id']};")
    # Deletes all created cards after a test:
    cursor.execute(f"delete from cards;")

@pytest.fixture
def sql_note(cursor, sql_card):
    """
    Insert note to be tested, and delete it after tests.
    """
    inserted_notes = []

    def _note(nid=1, mid=5, with_card=True):
        # default mid is 5 because "Basic" model id is 5 after restore_collection(...) fixture
        guid = f"guid{nid}"
        mod = 0
        usn = -1

        # Anki stores it's list of tags in plain text,
        # behind the scenes it uses the char ' ' (space) as a list item separator:
        tags = (" ").join([f"tag{nid}_1", f"tag{nid}_2"])

        # Anki stores it's list of fields in plain text,
        # behind the scenes it uses the char '\x1f' as a list item separator:
        flds = ("\x1f").join([f"field{nid} 1", f"field{nid} 2"])

        sfld = ""
        csum = 0

        # Anki-2.1.16 do not use the 'flags' and 'data' fields
        flags = 0
        data = ""

        sql_note_to_insert = f"""insert into
        notes (  id ,  guid  , mid , mod , usn ,  tags  ,  flds  ,  sfld ,  csum , flags ,  data  )
        values({nid},'{guid}',{mid},{mod},{usn},'{tags}','{flds}','{sfld}',{csum},{flags},'{data}')
        """
        sql_note = {"id": nid, "guid": guid, "mid": mid, "mod": mod, "usn": usn,
                    "tags": tags.split(" "), "flds": flds.split("\x1f"),
                    "sfld": sfld, "csum": csum, "flags": flags, "data": data}

        inserted_notes.append(sql_note)
        cursor.execute(sql_note_to_insert)

        if with_card is True:
            sql_card(cid=nid, nid=nid)

        return sql_note

    yield _note

    for note in inserted_notes:
        cursor.execute(f"delete from notes where id = {note['id']};")
    # Deletes all created notes after a test:
    cursor.execute(f"delete from notes;")

DEFAULT_NB_NOTES_CEIL = 7 # not included
DEFAULT_NB_NOTES_FLOOR = 1 # included

@pytest.fixture()
def sql_notes(sql_note):
    """
    Insert notes to be tested, and delete them after tests.
    """
    sql_notes = []
    for i in range(DEFAULT_NB_NOTES_FLOOR, DEFAULT_NB_NOTES_CEIL):
        #sql_note = insert_note(db, cursor, sql_basic_mid, i)
        note = sql_note(nid=i)
        sql_notes.append(note)
    yield sql_notes

@pytest.fixture()
def sql_notes_without_cards(sql_note):
    """
    Insert notes without cards to be tested, and delete them after tests.
    """
    sql_notes = []
    for i in range(DEFAULT_NB_NOTES_FLOOR, DEFAULT_NB_NOTES_CEIL):
        #sql_note = insert_note(db, cursor, sql_basic_mid, i)
        note = sql_note(nid=i, with_card=False)
        sql_notes.append(note)
    yield sql_notes

@pytest.fixture()
def sql_parse_tags():
    """
    Anki has a specific way to parse tags.
    """
    def _parse_tags(tags):
        return [t for t in tags.replace('\u3000', ' ').split(" ") if t]
    yield _parse_tags

@pytest.fixture(autouse=True)
def wipe_notes_and_cards_after_every_test(cursor):
    """
    Removes all notes and cards (that might have been created during a test).
    """
    yield
    cursor.execute("delete from notes;")
    cursor.execute("delete from cards;")

@pytest.fixture(scope="session", autouse=True)
def restore_collection(cursor, db, ankiwrap):
    """
    Remove extra content from the tests database collection in order to restore a "default" db.
    """

    # remove all graves: (https://github.com/ankidroid/Anki-Android/issues/5070)
    cursor.execute("delete from graves;")

    # put already synced notes into graves:
    cursor.execute("select id, usn from notes;")
    removed_notes = cursor.fetchall()
    for (nid, usn) in removed_notes:
        if usn > 0:
            cursor.execute(f"insert into graves (usn,oid,type) values ('-1','{nid[0]}','1');")

    # remove all notes:
    cursor.execute("delete from notes")

    # put already synced cards into graves:
    cursor.execute("select id, usn from cards;")
    removed_cards = cursor.fetchall()
    for (cid, usn) in removed_cards:
        if usn > 0:
            cursor.execute(f"insert into graves (usn,oid,type) values ('-1','{cid[0]}','0');")

    # remove all cards:
    cursor.execute("delete from cards;")

    # remove all decks except the "Default" deck:
    with open(TESTS_PATH / "default_decks.json", "r") as decks_file:
        default_decks = decks_file.read()
        #default_decks = default_decks.replace('\n', ' ').replace('\r', '')
        default_decks = " ".join(default_decks.split())
        cursor.execute(f"update col set models = '{default_decks}'")

    # remove all models except the defaults ones (NOTE that the "Basic" model id is 5)
    with open(TESTS_PATH / "default_models.json", "r") as models_file:
        default_models = models_file.read()
        #default_models = default_models.replace('\n', ' ').replace('\r', '')
        default_models = " ".join(default_models.split())
        sql = (f"update col set models = '{default_models}'")
        cursor.execute(sql)

    # remove all revlog:
    cursor.execute("delete from revlog;")

    # commit changes:
    db.commit()

    # reload collection with changes:
    ankiwrap.col.load()
