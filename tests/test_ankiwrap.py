"""
AnkiWrapper tests.
"""

import time
import threading

import click
import pytest
from pynput.keyboard import Key, Controller
from src.ankiwrap import *

# pylint: disable=W0611 # disable "unused import"
from global_fixtures import *
# pylint: enable=W0611 # enalbe back "unused import"

# Decks
################################################################################

# Models
################################################################################

# Notes
################################################################################

@pytest.mark.usefixtures("ankiwrap")
def test_find_notes(ankiwrap):
    """ Test how notes are found. """
    # See tests/test_notes_utils.py test_find_notes...(...) for more detailed tests.

    try:
        ankiwrap.find_notes(ids=False, flt=None, raw=False, qry=None)
    except Exception as e:
        raise pytest.fail("Anki-wrapper find_notes(...) raised an exception: {0}".format(e))

# Cards
################################################################################

@pytest.mark.usefixtures("ankiwrap")
def test_find_cards(ankiwrap):
    """ Test how cards are found. """
    # See test_cards_utils.py test_find_cards...(...) for more detailed tests.

    try:
        ankiwrap.find_cards(ids=False, flt=None, raw=False, qry=None)
    except Exception as e:
        raise pytest.fail("Anki-wrapper find_cards(...) raised an exception: {0}".format(e))

# Sync
################################################################################

def enter_credentials_thread(fake_user_e_mail, fake_password):
    """ Enter user crendentials in a seperate thread, simulating user keyboard. """

    # TODO
    # Is there a better way of doing this than directly simulating user keyboard?
    # This method: https://stackoverflow.com/a/36491341 works well for the input() funciton,
    # but it is limited to stdin, unfortunately the getpass() function is writing to /dev/tty
    # (https://docs.python.org/3/library/getpass.html), and I can't find which tty...
    # It is possible to tell getpass() to write to stderr instead of /dev/tty, but would it be
    # wise in terms of security?

    keyboard = Controller()

    time.sleep(0.1)
    keyboard.type(fake_user_e_mail)
    keyboard.press(Key.enter)
    keyboard.release(Key.enter)

    time.sleep(0.1)
    keyboard.type(fake_password)
    keyboard.press(Key.enter)
    keyboard.release(Key.enter)

@pytest.mark.usefixtures("pytestconfig")
def test_ask_user_credentials(pytestconfig):
    """ Test how the credentials are asked. """

    # Stop capturing stdout for cli prompt testing:
    # https://github.com/pytest-dev/pytest/issues/1599
    capmanager = pytestconfig.pluginmanager.getplugin('capturemanager')
    capmanager.suspend_global_capture(in_=True)

    print("\n")
    print("-----------------------------------------------------------------------")
    print("Stop capturing stdout for credentials prompt testing.")
    print("-----------------------------------------------------")

    thread = threading.Thread(target=enter_credentials_thread, args=("plop", ""))
    thread.start()
    with pytest.raises(click.exceptions.Abort):
        ask_user_credentials()

    thread = threading.Thread(target=enter_credentials_thread, args=("", "plop"))
    thread.start()
    with pytest.raises(click.exceptions.Abort):
        ask_user_credentials()

    thread = threading.Thread(target=enter_credentials_thread, args=("", ""))
    thread.start()
    with pytest.raises(click.exceptions.Abort):
        ask_user_credentials()

    thread = threading.Thread(target=enter_credentials_thread, args=("plop", "polp"))
    thread.start()
    assert ask_user_credentials() == ("plop", "polp")

    print("------------------------")
    print("Resume capturing stdout.")
    print("-----------------------------------------------------------------------")

    # Resume capturing stdout:
    capmanager.resume_global_capture()
