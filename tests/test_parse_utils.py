"""
Parse utility functions tests.
"""

import json

import src.parse_utils as putils

# pylint: disable=W0611 # disable "unused import"
from global_fixtures import *
# pylint: enable=W0611 # enalbe back "unused import"

# TODO test note parsing with multiple notes on the same line
# TODO test if missing start_delimiter
# TODO test if missing end_delimiter
# TODO test with multiple wrong json syntaxes
# TODO test note, card, deck, model... on the same line !

@pytest.mark.parametrize("content", [
    '<!--note {"unknown": 42, "flds": ["f", "b"]} -->',
    '<!--note {"unknown": 42, "flds": [\n"f", \n"b"\n]}-->',
    '<!--note {"unknown": 42, "flds": ["f", "b"]}\n-->',
    '<!--note\n{"unknown": 42, "flds": ["f", "b"]} -->',
    '<!--note\n {"unknown": 42, "flds": ["f", "b"]} \n-->',
    ])
def test_parse_file_with_unknown_json_key(ankiwrap, tmp_path, content):
    """
    Test parse_file(...) with an unknown json key.
    This test expects to raise an exception when parse_file(...) is given an unknown key.
    """
    file_path = tmp_path / "file_to_parse.md"
    file_path.write_text(content)
    with pytest.raises(Exception):
        putils.parse_file(col=ankiwrap.col, file_path=file_path)

@pytest.mark.parametrize("content", [
    '<!--note {"id": 42, "id": 36, "flds": ["f", "b"]} -->',
    '<!--note {"id": 42, "id": 36, "flds": [\n"f", \n"b"\n]}-->',
    '<!--note {"id": 42, "id": 36, "flds": ["f", "b"]} \n-->',
    '<!--note\n {"id": 42, "id": 36, "flds": ["f", "b"]} -->',
    '<!--note\n{"id": 42, "id": 36, "flds": ["f", "b"]}\n-->',
    ])
def test_parse_file_with_duplicated_json_key(ankiwrap, tmp_path, content):
    """
    Test parse_file(...) with a duplicated json key.
    This test expects to raise an exception when parse_file(...) is given the same key twice.
    """
    file_path = tmp_path / "file_to_parse.md"
    file_path.write_text(content)
    with pytest.raises(Exception):
        putils.parse_file(col=ankiwrap.col, file_path=file_path)

@pytest.mark.parametrize("content", [
    '<!--note {"flds": ["f", "b"]} -->',
    '<!--note {"flds": [\n"f", \n"b"\n]}-->',
    '<!--note {"flds": ["f", "b"]} \n-->',
    '<!--note\n {"flds": ["f", "b"]} -->',
    '<!--note\n{"flds": ["f", "b"]}\n-->',
    ])
def test_parse_file_with_no_json_nid_key(ankiwrap, tmp_path, content):
    """
    Test parse_file(...) with no json nid key.
    This test expects to raise an exception when parse_file(...) isn't given a nid.
    """
    file_path = tmp_path / "file_to_parse.md"
    file_path.write_text(content)
    with pytest.raises(Exception):
        putils.parse_file(col=ankiwrap.col, file_path=file_path)

@pytest.mark.parametrize("content", [
    '<!--note {"flds": ["f", "b"]}-->'\
    '<!--note {"flds": ["f", "b"]}-->',
    '<!--note {"flds": ["f", "b"]}--><!--note {"flds": ["f", "b"]}-->',
    ])
def test_parse_file_with_multiple_attached_blocks(ankiwrap, tmp_path, content):
    """
    Test parse_file(...) with two blocks attached to each other on the same line.
    This test expects to raise an exception when parse_file(...) encounter attached blocks.
    """
    file_path = tmp_path / "file_to_parse.md"
    file_path.write_text(content)
    with pytest.raises(Exception):
        putils.parse_file(col=ankiwrap.col, file_path=file_path)

@pytest.mark.parametrize(("nid1", "nid2", "nid3", "content", "mid"), [
    # single add tests:
    (1, -1, -1, '<!--note {"id": 1, "mid": 4, "flds": ["f", "b"]} -->', 4),
    (2, -1, -1, '<!--note {"id": 2, "mid": 4, "flds": [\n"f", \n"b"\n]}-->', 4),
    (3, -1, -1, '<!--note {"id": 3, "mid": 4, "flds": ["f", "b"]}\n-->', 4),
    (4, -1, -1, '<!--note\n{"id": 4, "mid": 4, "flds": ["f", "b"]} -->', 4),
    (5, -1, -1, '<!--note\n {"id": 5, "mid": 4, "flds": ["f", "b"]} \n-->', 4),
    # multiple add tests:
    (1, 2, 3, '<!--note {"id": 1, "mid": 4, "flds": ["f", "b"]} --> '\
              '<!--note {"id": 2, "mid": 4, "flds": ["f", "b"]}--> '\
              '<!--note {"id": 3, "mid": 4, "flds": ["f", "b"]}-->', 4),
    (4, 5, 6, '<!--note {"id": 4, "mid": 4, "flds": ["f", "b"]}\n--> '\
              '<!--note\n{"id": 5, "mid": 4, "flds": ["f", "b"]} --> '\
              '<!--note\n {"id": 6, "mid": 4, "flds": [\n"f", \n"b"\n]} \n-->', 4),
    # single udpates tests:
    (1, -1, -1, '<!--note {"id": 1, "mid": 4, "flds": ["f", "b"]}--> '\
                '<!--note {"id": 1, "mid": 5, "flds": ["f", "b"]}-->', 5),
    (2, -1, -1, '<!--note {"id": 2, "mid": 5, "flds": ["f", "b"]}--> '\
                '<!--note\n {"id": 2, "mid": 4, "flds": [\n"f", \n"b"\n]} \n-->', 4),
    # multiple udpates tests:
    (1, -1, -1, '<!--note {"id": 1, "mid": 4, "flds": ["f", "b"]}--> '\
                '<!--note {"id": 1, "mid": 5, "flds": ["f", "b"]}--> '\
                '<!--note {"id": 1, "mid": 4, "flds": ["f", "b"]}--> '\
                '<!--note {"id": 1, "mid": 5, "flds": ["f", "b"]}-->', 5),
    (2, -1, -1, '<!--note {"id": 2, "mid": 5, "flds": ["f", "b"]}\n--> '\
                '<!--note\n{"id": 2, "mid": 4, "flds": ["f", "b"]}--> '\
                '<!--note\n{"id": 2, "mid": 5, "flds": ["f", "b"]}\n--> '\
                '<!--note\n {"id": 2, "mid": 4, "flds": [\n"f", \n"b"\n]} \n-->', 4),
    ])
def test_parse_file_for_mid(ankiwrap, cursor, tmp_path, nid1, nid2, nid3, mid, content):
    """
    Test parse_file(...) by creating/updating note(s) (based on file/content) with specific mid.
    This test expects to find the created/updated note(s) and make sure its mid is the rigth one.
    """
    file_path = tmp_path / "file_to_parse.md"
    file_path.write_text(content)
    putils.parse_file(col=ankiwrap.col, file_path=file_path)
    cursor.execute(f"select mid from notes where id = {nid1}")
    sql_mid = cursor.fetchone()[0]
    assert sql_mid == mid
    if nid2 != -1:
        cursor.execute(f"select mid from notes where id = {nid2}")
        sql_mid2 = cursor.fetchone()[0]
        assert sql_mid2 == mid
    if nid3 != -1:
        cursor.execute(f"select mid from notes where id = {nid3}")
        sql_mid3 = cursor.fetchone()[0]
        assert sql_mid3 == mid


@pytest.mark.usefixtures("sql_decks")
@pytest.mark.parametrize(("mid", "did", "content"), [
    # single add tests:
    (5, 1, '<!--note {"id": 1, "mid": 5, "did": 1, "flds": ["f", "b"]} -->'),
    (5, 2, '<!--note {"id": 2, "mid": 5, "did": 2, "flds": [\n"f", \n"b"\n]}-->'),
    (5, 3, '<!--note {"id": 3, "mid": 5, "did": 3, "flds": ["f", "b"]}\n-->'),
    (5, 4, '<!--note\n{"id": 4, "mid": 5, "did": 4, "flds": ["f", "b"]} -->'),
    (5, 5, '<!--note\n {"id": 5, "mid": 5, "did": 5, "flds": ["f", "b"]} \n-->'),
    # multiple add tests:
    (5, 1, '<!--note {"id": 1, "mid": 5, "did": 1, "flds": ["f", "b"]} --> '\
           '<!--note {"id": 2, "mid": 5, "did": 1, "flds": ["f", "b"]}--> '\
           '<!--note {"id": 3, "mid": 5, "did": 1, "flds": ["f", "b"]}-->'),
    (5, 2, '<!--note {"id": 4, "mid": 5, "did": 2, "flds": ["f", "b"]}\n--> '\
           '<!--note\n{"id": 5, "mid": 5, "did": 2, "flds": ["f", "b"]} --> '\
           '<!--note\n {"id": 6, "mid": 5, "did": 2, "flds": [\n"f", \n"b"\n]} \n-->'),
    # single updates tests:
    (5, 2, '<!--note {"id": 1, "mid": 5, "did": 1, "flds": ["f", "b"]}--> '\
           '<!--note {"id": 1, "mid": 5, "did": 2, "flds": [\n"f", \n"b"\n]}-->'),
    (5, 4, '<!--note\n{"id": 2, "mid": 5, "did": 3, "flds": ["f", "b"]}\n--> '\
           '<!--note\n {"id": 2, "mid": 5, "did": 4, "flds": [\n"f", \n"b"\n]} \n-->'),
    # multiple updates tests:
    (5, 4, '<!--note {"id": 1, "mid": 5, "did": 1, "flds": ["f", "b"]}--> '\
           '<!--note {"id": 1, "mid": 5, "did": 2, "flds": ["f", "b"]}--> '\
           '<!--note {"id": 1, "mid": 5, "did": 3, "flds": ["f", "b"]}--> '\
           '<!--note {"id": 1, "mid": 5, "did": 4, "flds": ["f", "b"]}-->'),
    (5, 2, '<!--note {"id": 2, "mid": 5, "did": 5, "flds": ["f", "b"]}\n--> '\
           '<!--note\n{"id": 2, "mid": 5, "did": 4, "flds": ["f", "b"]}--> '\
           '<!--note\n{"id": 2, "mid": 5, "did": 3, "flds": ["f", "b"]}\n--> '\
           '<!--note\n {"id": 2, "mid": 5, "did": 2, "flds": [\n"f", \n"b"\n]} \n-->'),
    ])
def test_parse_file_for_did(ankiwrap, cursor, tmp_path, mid, did, content):
    """
    Test parse_file(...) by creating/updating note(s) (based on the file/content) with specific did.
    This test expects to find the created/updated note(s) and make sure did is the rigth one.
    """
    file_path = tmp_path / "file_to_parse.md"
    file_path.write_text(content)
    putils.parse_file(col=ankiwrap.col, file_path=file_path)
    cursor.execute("select models from col") # get all models
    models = cursor.fetchone()[0] # extract models (in anki db struct: there is only one 'col' raw)
    models_json = json.loads(models) # load models (models_json is a dict)
    fetched_did = -1
    for model in models_json:
        if int(models_json[model]["id"]) == mid:
            fetched_did = models_json[model]["did"]
            break
    assert fetched_did == did
