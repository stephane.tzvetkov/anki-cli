# Install, uninstall, update

!!! warning "Prerequisiste"
    `Python` (**`v3`**) needs to be installed (cf. https://www.python.org/about/gettingstarted/)

## Install

**WIP**

> Note that if you are looking to work on anki-cli project itself, you might consider the
> [developper way](../developer-guide/install_uninstall_update.md) of
> installing/uninstalling/updating the project.

* Clone the `anki-cli` repository and change directory into it:
```console
$ git clone --recurse-submodules https://gitlab.com/stephane.tzvetkov/anki-cli.git
$ cd anki-cli
```

* [Optional] Create a python virtual environement and activate it:
```console
$ python -m venv pyenv
$ source ./pyenv/bin/activate
```

* Update `pip` and install `anki-cli` (it will auto-install its dependencies, cf. `setup.py`):
```console
$ python -m pip install --upgrade pip
$ ython3 -m pip install .
```

* Now `anki-cli` can be run like so:
```console
$ anki-cli --help
```

## Uninstall

**WIP**

```console
$ python -m pip uninstall anki-cli
```

## Update

**WIP**

```console
$ git pull
$ git submodule update --init --recursive
```

