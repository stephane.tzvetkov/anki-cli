# Get started and contribute

Here is a very good reference to get started with Anki in general:

* https://docs.ankiweb.net/#/getting-started

More interesting links:

* https://github.com/ankidroid/Anki-Android/wiki/Database-Structure
* https://repology.org/project/sqlitebrowser/versions
