# Project layout

**WIP**

```console
$ tree . -a -I ".coverage*|.git*|.gitignore*|.gitmodules*|.pytest_cache*|anki_cli.egg-info*|pyenv*|__pycache__*|tests-base*" -L 2
.
├── anki-2_1_16-submodule   # git submodule including the original anki project (v2.1.16)
│   └── ...
├── docs                    # documentation folder, build by mkdocs for readthedocs
│   └── ...
├── LICENSE.md              # MIT license details
├── mkdocs.yml              # mkdocs (tool to build the documentation) config file
├── parsing-example.md      # example of a parsable file to create decks, models, cards, notes...
├── pylintrc                # pylint config file
├── README.md               # the current file you are browsing
├── .readthedocs.yml        # readthedocs config file
├── setup.py                # python script distributing the anki-cli module (`$ pip install .`)
├── site                    # documentation site, generated and consultable with mkdocs
│   └── ...
├── src                     # source code folder
│   ├── anki_cli.py         # entry point of anki-cli
│   ├── ankiwrap.py         # anki wrapper for anki-cli
│   ├── cards_utils.py      # cards utility functions for ankiwrap
│   ├── click_utils.py      # click utility functions for ankiwrap
│   ├── decks_utils.py      # decks utility functions for ankiwrap
│   ├── __init__.py         # anki-cli module declaration, importing globally anki submodule
│   ├── notes_utils.py      # notes utility functions for ankiwrap
│   ├── parse_utils.py      # parse utility functions for ankiwrap
│   ├── profiles.py         # anki profile manager, without qt stuff, ported for anki-cli
│   └── sync.py             # anki sync manager, without qt stuff, ported for anki-cli
└── tests                   # tests folder
    ├── conftest.py         # pytest configuration file
    ├── default_decks.json  # default content of an anki-deck
    ├── default_models.json # default content of an anki-model
    ├── global_fixtures.py  # pytest fixtures of this test directory
    ├── pylintrc            # pylint specific config file for tests
    ├── pytest.ini          # pytest init file
    ├── test_anki_cli.py    # test code for ../src/anki_cli.py
    ├── test_ankiwrap.py    # test code for ../src/ankiwrap.py
    ├── test_cards_utils.py # test code for ../src/cards_utils.py
    ├── test_decks_utils.py # test code for ../src/decks_utils.py
    ├── test_notes_utils.py # test code for ../src/notes_utils.py
    └── test_parse_utils.py # test code for ../src/parse_utils.py
```
