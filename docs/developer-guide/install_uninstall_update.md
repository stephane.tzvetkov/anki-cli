# Install, uninstall, update

!!! warning "Prerequisiste"
    `Python` (**`v3`**) needs to be installed (cf. https://www.python.org/about/gettingstarted/)

## Install

* Clone the `anki-cli` repository and change directory into it:
```console
$ git clone --recurse-submodules https://gitlab.com/stephane.tzvetkov/anki-cli.git
$ cd anki-cli
```

* [Optional] Create a python virtual environement and activate it:
```console
$ python -m venv pyenv
$ source ./pyenv/bin/activate
```

* Update `pip` (if needed):
```console
$ python -m pip install --upgrade pip
```

* Install `anki-cli`, including dev dependencies (cf. `setup.py`):
```console
(zsh) $ python -m pip install -e .\[dev\]
or
(bash) $ python -m pip install -e .[dev]
```

* Now `anki-cli` can be run like so:
```console
$ anki-cli --help
or
$ python ./src/anki_cli.py --help # if you didn't used the `-e` option when doing `pip install`
```

## Uninstall

**WIP**

```console
$ python -m pip uninstall anki-cli
```

## Update

**WIP**

```console
$ git pull
$ git submodule update --init --recursive
```

