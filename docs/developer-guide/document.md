# Document

**WIP**

* Build the documentation:
```console
$ mkdocs build -f .mkdocs.yml
```

* Build and start a live-reloading documentation server:
```console
$ mkdocs serve -f .mkdocs.yml
    > ...
    > INFO    -  Serving on http://127.0.0.1:8000
    > ...
```

!!! tip "Tip"
    You can use [`linkchecker`](https://linkchecker.github.io/linkchecker/) in order to make sure
    there is no broken links on the generated and running website: `$ linkchecker
    http://127.0.0.1:8000`
