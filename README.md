⚠️ **DEPRECATED** ⚠️

The following tools are used instead:
- [ki](https://github.com/langfield/ki)
- [toml2anki](https://gitlab.com/stephane.tzvetkov/toml2anki)

---
> # ~~anki-cli~~
> 
> ~~anki-cli aims to be a command line interface alternative to the Anki desktop application.~~
> 
> ~~Anki is a free and open-source flashcard program that utilizes spaced repetition in order to~~
> ~~memorize many things easily (cf. *https://docs.ankiweb.net/#/* and~~
> ~~*https://apps.ankiweb.net/index.html*).~~
> 
> ---
> ## Disclaimer
> 
> * ~~I'm not a professional `Python` developer, please help me increase the code quality of~~
>   ~~anki-cli.~~
> * ~~This software is currently **WORK IN PROGRESS**: it is unstable, untested and not even *really*~~
>   ~~functional... Use it at your own risk, I encourage you to make frequent backups.~~
> 
> ---
> ## ~~Table of contents~~
> 
> <!-- vim-markdown-toc GitLab -->
> 
> * [~~Sources and motivations~~](#sources-and-motivations)
> * [~~Status~~](#status)
> * [~~Prerequisiste(s)~~](#prerequisistes)
> * [~~User guide~~](#user-guide)
> * [~~Developer guide~~](#developer-guide)
> * [~~TODO / features~~](#todo-features)
>     * [~~High priority~~](#high-priority)
>     * [~~Low priority~~](#low-priority)
> * [~~WIP notes~~](#wip-notes)
> * [~~Legal requirements~~](#legal-requirements)
> 
> <!-- vim-markdown-toc -->
> 
> ---
> ## ~~Sources and motivations~~
> 
> ~~This tool is heavily inspired by the following projects, check them, they might be enough for what~~
> ~~you need:~~
> 
> * ~~https://github.com/patarapolw/ankisync2~~
> * ~~https://github.com/patarapolw/ankisync~~
> * ~~https://github.com/patarapolw/AnkiTools~~
> * ~~https://github.com/lervag/apy~~
> * ~~https://github.com/FooSoft/anki-connect~~
> * ~~https://github.com/kerrickstaley/genanki~~
> * ~~https://github.com/sdondley/Anki-Import~~
> * ~~https://github.com/MFreidank/AnkiVim~~
> * ~~https://github.com/tbabej/knowledge~~
> 
> ~~As interesting as all these projects are, they all require the official Anki gui application to be~~
> ~~running and/or to be installed (at least at some point, e.g. to identify and connect before the~~
> ~~first sync). Moreover, many of them are no longer actively maintained.~~
> 
> ~~This project tries to provide an alternative, as full-featured as possible, addressing these~~
> ~~aspects.~~
> 
> ---
> ## ~~Status~~
> 
> * ~~anki-cli is now build against [anki 2.1.16](https://github.com/ankitects/anki/tree/2.1.16)~~
>   ~~(commit 4bc33e2f), let's hope this version remains compatible with the~~
>   ~~[last official anki](https://github.com/ankitects/anki) long enough not to worry about it~~
>   ~~(too much).~~
> 
> * ~~**This application is a work in progress, it is far from being functional and even farther from~~
>   ~~being considered reliable.**~~
> 
> ---
> ## ~~Prerequisiste(s)~~
> 
> * ~~`Python` (**`v3`**) needs to be installed (cf. https://www.python.org/about/gettingstarted/)~~
> 
> ---
> ## ~~User guide~~
> 
> * ~~[How to install? How to uninstall? How to update?](./user-guide/install_uninstall_update/)~~
> * ~~[How to use?](https://anki-cli.readthedocs.io/en/latest/user-guide/use/)~~
> 
> ---
> ## ~~Developer guide~~
> 
> * ~~[Project layout](./developer-guide/project_layout.md)~~
> * ~~[How to install? How to uninstall? How to update?](./developer-guide/install_uninstall_update.md)~~
> * ~~[How to get started? How to contribute?](./developer-guide/get_started_and_contribute.md)~~
> * ~~[How to debug?](./developer-guide/debug.md)~~
> * ~~[How to test?](./developer-guide/test.md)~~
> * ~~[How to document?](./developer-guide/document.md)~~
> 
> ---
> ## ~~TODO / features~~
> 
> ### ~~High priority~~
> 
> * ~~[ ] avoid anki-submodule dependency: become anki compatible without relying on the anki project~~
> * ~~[ ] anki-cli.conf file (containing various settings)~~
> * ~~[ ] sync~~
>     * ~~[ ] sign in / sign out~~
>     * ~~[ ] sync triggering (remote AnkiWeb content becomes the same as the local one and vice versa)~~
>     * ~~[ ] sync conflicts management (download and override, or upload and override, backup anyway)~~
> * ~~[ ] backups (by default, backup last 10 sync Anki2 dirs, and last 10 manually triggered backups)~~
>     * ~~[ ] backups settings: backups location, max number of backups~~
>     * ~~[ ] trigger backup~~
>     * ~~[ ] restore backup~~
> * ~~[ ] full local mode (without AnkiWeb account, so without sync)~~
>     * ~~[ ] ability to create and use a non-AnkiWeb account~~
> * ~~[ ] notes~~
>     * ~~[x] find note(s) with filtered search~~
>         * ~~[x] support for anki built-in query system: https://docs.ankiweb.net/#/searching~~
>     * ~~[x] create a note (beware of duplicates)~~
>     * ~~[x] delete a note (with confirmation option)~~
>     * ~~[x] update a note (w/o altering it's associated card's stats, or resetting stats if wanted)~~
>         * ~~[ ] edit mode (parser needed)~~
>     * ~~[x] add tag(s) (beware of duplicates)~~
>     * ~~[ ] delete tag(s) (with confirmation option)~~
> * ~~[ ] cards~~
>     * ~~[x] find card(s) with filtered search~~
>         * ~~[x] support for anki built-in query: https://docs.ankiweb.net/#/searching~~
>     * ~~[ ] create a card (beware of duplicates)~~
>     * ~~[ ] delete a card (with confirmation option)~~
>     * ~~[ ] update a card~~
>         * ~~[ ] edit mode (parser needed)~~
> * ~~[ ] models~~
>     * ~~[x] print models~~
>     * ~~[ ] find model(s) with filtered search~~
>         * ~~[ ] support for anki built-in query system: https://docs.ankiweb.net/#/searching ?~~
>     * ~~[ ] create a model (beware of duplicates)~~
>     * ~~[ ] delete a model (with confirmation option)~~
>     * ~~[ ] update a model~~
>         * ~~[ ] edit mode (parser needed)~~
> * ~~[ ] decks~~
>     * ~~[ ] list decks or find deck(s) with filtered search~~
>         * ~~[ ] support for anki built-in query system: https://docs.ankiweb.net/#/searching ?~~
>     * ~~[ ] create a deck (beware of duplicates)~~
>     * ~~[ ] delete a deck (with confirmation option)~~
>     * ~~[ ] update a deck (add cards / remove cards)~~
>     * ~~[ ] import a deck (beware of conflicting notes id, did, mid etc.)~~
>     * ~~[ ] export a deck~~
>     * ~~[ ] support deck configuration (dconf in col table)~~
>     * ~~[ ] support subdecks and superdecks~~
> * ~~[ ] file parser for deck management~~
>     * ~~[ ] define how a file can be parsed (syntax, examples) and document it~~
>     * ~~[ ] pretend flag: show what would have happened if the parsing was for real.~~
>     * ~~[ ] fields to create/update deck, model(s), card(s), note(s) (beware of duplicated ids)~~
>     * ~~[ ] field to define default deck~~
>     * ~~[ ] field to define default model id~~
>     * ~~[ ] field to override default card~~
>     * ~~[ ] auto card (new note will also create new card on the fly, based on the default card)~~
>     * ~~[ ] cards stats: by default cards stats are not altered, but they could be reset if wanted~~
>     * ~~[ ] auto note id (note id will be auto-write by the parser to the file, enabled by default)~~
>     * ~~[ ] override flag: must be specified to allow overrides (updates and deletions) with a~~
>           ~~warning and confirmation msg. -y --yes flag to auto confirm.~~
>     * ~~[ ] is there a way to make sure a parsed note is beeing correctly modified and not~~
>           ~~accidentaly overrided (e.g. because of an mistake in the note id)?~~
>           ~~==> a `last-user` field to parse.~~
>     * ~~[ ] field to define the `last-user` of the file, raising confirmation prompt if the last user~~
>           ~~do not match the current anki-cli user~~
>     * ~~[ ] print warning and confirmation msg, if the `last-user` do not match the current user~~
>     * ~~[ ] auto update/add `last-user` field (at the top of the file), based on AnkiWeb user name.~~
>           ~~Enabled by default: -a --auto-last-user false, to avoid default~~
>     * ~~[ ] prefix ids: prefix all the following ids by this int (block to parse). Prefix ids is auto~~
>           ~~generated by the parser by default (parser will write it at the top of the file) based on~~
>           ~~the file name (/!\ duplicates). -i --auto-ids false, to avoid default~~
>     * ~~[ ] new file check command to warn about duplicated ids and other errors and incoherences.~~
>     * ~~[ ] vim snippet to insert note easily~~
> 
> ### ~~Low priority~~
> 
> * ~~[ ] users~~
>     * ~~[ ] multiple users/accounts~~
>     * ~~[ ] list users~~
>     * ~~[ ] select user~~
>     * ~~[ ] change user name~~
>     * ~~[ ] add user (beware of duplicates)~~
>     * ~~[ ] delete user (with confirmation option)~~
> * ~~[ ] sql~~
>     * ~~[ ] execute sql command (with confirmation option)~~
>     * ~~[ ] execute script (with confirmation option)~~
> * ~~[ ] review~~
>     * ~~[ ] interactive deck(s) review ?~~
>     * ~~maybe this feature should be set aside for now,~~
> * ~~[ ] statistics~~
>     * ~~[ ] print card(s) stats ?~~
> * ~~[ ] explain (in help pages? in a separated doc?) a little bit more the data structure:~~
>     * ~~https://github.com/ankidroid/Anki-Android/wiki/Database-Structure~~
>     * ~~https://github.com/ankidroid/Anki-Android/wiki/Database-Structure~~
>     * ~~https://web.archive.org/web/20190309092907/https://github.com/ankidroid/Anki-Android/wiki/Database-Structure~~
>     * ~~https://docs.ankiweb.net/#/~~
> * ~~[ ] badges (https://medium.com/@iffi33/adding-custom-badges-to-gitlab-a9af8e3f3569 https://shields.io/)~~
>     * ~~[x] "read the doc" badge (e.g. https://readthedocs.org/projects/pytest-cov/) with commands'~~
>           ~~help content auto-filled from the doc section~~
>     * ~~[x] MIT license badge~~
>     * ~~[ ] requirements badge (e.g. https://requires.io/github/pytest-dev/pytest-cov/requirements/?branch=master)~~
>     * ~~[x] code coverage badge~~
>     * ~~[ ] builds badge~~
>     * ~~[ ] version badge (pypi) / tag badge (https://github.com/qmk/qmk_firmware)~~
>     * ~~[ ] downloads count badge~~
>     * ~~[ ] nb of lines of code badge~~
> * ~~[ ] dynamic decks?~~
> * ~~[ ] filtered decks (https://docs.ankiweb.net/#/filtered-decks)~~
> * ~~[ ] media support (images and sounds)~~
> * ~~[ ] support more searches (cf. find.py)?~~
> * ~~[ ] read more the anki core code to find more nice functionnalities to support~~
> * ~~[ ] https://github.com/click-contrib~~
> 
> ---
> ## ~~WIP notes~~
> 
> * ~~ids (decks, notes, cards, models) smaller than or equal to 0 must be forbidden.~~
> 
> ---
> ## ~~Legal requirements~~
> 
> ~~According to [this](https://www.tldrlegal.com/l/mit) and [this](https://www.tldrlegal.com/l/mit)~~
> ~~summurized licenses descriptions (respectively the licences used by the anki-cli project and the~~
> ~~anki project), here are the legal requirements of the anki-cli project:~~
> 
> * ~~The [anki-cli license](https://gitlab.com/stephane.tzvetkov/anki-cli/-/blob/master/LICENSE.md)~~
>   ~~([MIT](https://opensource.org/licenses/mit-license.php)) is compatible with the [anki v2.1.16 license](https://github.com/ankitects/anki/blob/2.1.16/LICENSE)~~
>   ~~([AGPL3](https://www.gnu.org/licenses/agpl-3.0.html)), cf. [wikipedia about license compatibility](https://en.wikipedia.org/wiki/License_compatibility#Compatibility_of_FOSS_licenses).~~
> 
> * ~~The anki-cli project uses source code from the [anki v2.1.16 repository](https://github.com/ankitects/anki/blob/2.1.16/LICENSE). This repository is imported~~
>   ~~in the anki-cli project as a [git submodule](https://git-scm.com/docs/git-submodule), including~~
>   ~~it's copyright: "Ankitects Pty Ltd and contributors", and it's~~
>   ~~[license](https://github.com/ankitects/anki/blob/2.1.16/LICENSE).~~
> 
> * ~~A smaller part of the [anki v2.1.16 source code](https://github.com/ankitects/anki/blob/2.1.16/LICENSE) is also imported and modified~~
>   ~~outside the git submodule: in the anki-cli source code directory "src" (note that this directory~~
>   ~~does not only contains modified anki v2.1.16 source code but also original anki-cli source code).~~
>   ~~Even if those source code files have been (sometimes extensively) modified, they always refer to~~
>   ~~their original file and always include the original license and their original copyright (the~~
>   ~~details of the changes made can be checked by comparing the modified and the originals files~~
>   ~~using a simple comparison tool, e.g. [diff](https://www.gnu.org/software/diffutils/)).~~
> 
> * ~~The significant changes made to the imported and modifed~~
>   ~~[anki v2.1.16](https://github.com/ankitects/anki/tree/2.1.16) source code consist in transforming anki~~
>   ~~from a graphical user interface (GUI) to a command line interface (CLI).~~
> 
> * ~~The anki-cli sources will always be available~~
>   ~~[here](https://gitlab.com/stephane.tzvetkov/anki-cli)~~
> 
> * ~~The build and install instructions are included in this document (cf. above "install"~~
>   ~~sub-sections).~~
